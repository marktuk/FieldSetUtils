/**
 *  __  __            _      _____                    _ _
 * |  \/  | __ _ _ __| | __ |_   _|   _ _ __ _ __ ___| | |
 * | |\/| |/ _` | '__| |/ /   | || | | | '__| '__/ _ \ | |
 * | |  | | (_| | |  |   <    | || |_| | |  | | |  __/ | |
 * |_|  |_|\__,_|_|  |_|\_\   |_| \__, |_|  |_|  \___|_|_|
 *                                |___/
 *
 * FieldSetUtils
 * Utils class for FieldSets on all objects
 * 
 */
public class FieldSetUtils {

    public static Boolean hasFieldSet(String objectName, String fieldSetName)
    {
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
        Map<String, Schema.FieldSet> fieldSets = objectDescribe.fieldSets.getMap();

        return fieldSets.containsKey(fieldSetName);
    }

    public static Schema.FieldSet getFieldSet(String objectName, String fieldSetName)
    {
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
        Map<String, Schema.FieldSet> fieldSets = objectDescribe.fieldSets.getMap();

        return fieldSets.get(fieldSetName);
    }

    public static List<String> getFieldList(String objectName, String fieldSetName)
    {
        Schema.FieldSet fieldSet = getFieldSet(objectName, fieldSetName);
        List<String> fieldList = new List<String>();

        for (Schema.FieldSetMember field : fieldSet.getFields())
        {
            fieldList.add(field.getFieldPath());
        }   

        return fieldList;
    }

    public static List<FieldSetMember> getFieldSetMembers(String objectName, String fieldSetName)
    {
        Schema.FieldSet fieldSet = getFieldSet(objectName, fieldSetName);
        List<FieldSetMember> fieldSetMembers = new List<FieldSetMember>();

        for (Schema.FieldSetMember field : fieldSet.getFields()) {
            fieldSetMembers.add(new FieldSetMember(field));
        }  

        return fieldSetMembers;   
    }

}
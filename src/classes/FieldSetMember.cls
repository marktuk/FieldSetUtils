/**
 *  __  __            _      _____                    _ _
 * |  \/  | __ _ _ __| | __ |_   _|   _ _ __ _ __ ___| | |
 * | |\/| |/ _` | '__| |/ /   | || | | | '__| '__/ _ \ | |
 * | |  | | (_| | |  |   <    | || |_| | |  | | |  __/ | |
 * |_|  |_|\__,_|_|  |_|\_\   |_| \__, |_|  |_|  \___|_|_|
 *                                |___/
 *
 * FieldSetMember
 * AuraEnabled FieldSetMember wrapper class
 * 
 */
public class FieldSetMember
{
    public FieldSetMember(Schema.FieldSetMember f)
    {
        this.DBRequired = f.DBRequired;
        this.fieldPath = f.fieldPath;
        this.label = f.label;
        this.required = f.required;
        this.type = '' + f.getType();
        this.value = '';
    }

    public FieldSetMember(Boolean DBRequired)
    {
        this.DBRequired = DBRequired;
    }

    @AuraEnabled
    public Boolean DBRequired { get; set; }

    @AuraEnabled
    public String fieldPath { get; set; }

    @AuraEnabled
    public String label { get; set; }

    @AuraEnabled
    public Boolean required { get; set; }

    @AuraEnabled
    public String type { get; set; }

    @AuraEnabled
    public String value { get; set; }
}